export default interface Produce {
  id: string;
  name: string;
  price: number;
  qty: number;
  photo: string;
}
